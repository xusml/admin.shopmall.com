import { defineStore } from "pinia";
import { ElMessage } from 'element-plus'
import axios from 'axios'
import { post } from '@/http/http'
import { useRoute } from 'vue-router'
import Cookie from 'js-cookie'
import router from "@/router";

export const useUserStore = defineStore({
    id: 'userStore',
    persist: true, //持久化
    state: () => ({
        user: new UserInfo()
    }),
    actions: {
        setUser(user: UserInfo) {
            this.user = user
        },
        loginOut() {
            let _this = this
            let data = {
                _gp: "admin",
                _mt: "logout"
            }
            post(data).then(res => {
                console.log(res);
                let { errno } = res
                if (200 == errno) {
                    Cookie.remove('token')//移除令牌
                    _this.user = new UserInfo()//初始化仓库
                    router.replace('/login') //跳转到登录页
                    ElMessage.success('退出成功')
                }
            }).catch(err => {
                ElMessage.error(err)
            })
        }
    }
})
class UserInfo {
    avatarUrl?: string;
    gmtCreate: number;
    gmtLastLogin: string;
    gmtUpdate: number;
    id: number;
    lastLoginIp: string;
    perms: string[];
    phone: string;
    realname: string;
    roleIds: number[];
    roles: string[];
    status: number;
    username: string;
    password: number | undefined;
    constructor() {
        this.avatarUrl = '';
        this.gmtCreate = 0;
        this.gmtLastLogin = '';
        this.gmtUpdate = 0;
        this.id = 0;
        this.lastLoginIp = '';
        this.perms = [];
        this.phone = '';
        this.realname = '';
        this.roleIds = [];
        this.roles = [];
        this.status = 0;
        this.username = '';
    }
}