import * as XLSX from "xlsx";
import { ElMessage } from "element-plus";
export default function deriveExcel() {
  let workbook = XLSX.utils.table_to_book(document.getElementById("table")); //需要在table上定义一个id
  try {
    XLSX.writeFile(workbook, "表格信息.xlsx");
    ElMessage({
      type: "success",
      message: "导出成功!",
    });
  } catch (e) {
    ElMessage.error("导出失败,失败信息:!");
  }
}
