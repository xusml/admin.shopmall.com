
// 日期封装 
export function goodDate(time: number) {
  let current = +new Date();//获取一个时间戳
  let distance = (current - time) / 1000; //差值转换为秒
  switch (true) {
    case distance < 30: //30s
      return "刚刚";
    case distance < 60 * 60: //60分钟
      return parseInt((distance / 60) as unknown as string) <= 1
        ? "1分钟内"
        : parseInt((distance / 60) as unknown as string) + "分钟前";
    case distance < 60 * 60 * 24: //24小时
      return parseInt((distance / 60 / 60) as unknown as string) + "小时前";
    case distance < 60 * 60 * 24 * 2: //2天
      return "2天前";
    case true: //年月日
      let date = new Date(time);
      let year = date.getFullYear();
      let mouth = (date.getMonth() + 1 + '').padStart(2, '0');//（padStart）补零，不超过2位才补
      let day = date.getDate();
      let hours = date.getHours();
      let Milliseconds = date.getMinutes();
      return (year + "年" + mouth + "月" + day + "日 " + hours + "时" + Milliseconds + "分");
    default:
      break;
  }
}

  
/**上次登录和生日的日期封装
 * */
export function loginDate(datas: string | undefined, symbol: string, bool?: boolean) {
  if (!datas) {
    return undefined
  }
  let date = new Date(datas);
  let yy = date.getFullYear() //年
  let mm = (date.getMonth() + 1 + '').padStart(2, '0') //月
  let dd = (date.getDate() + '').padStart(2, '0') //日
  let hh = (date.getHours() + '').padStart(2, '0'); //时
  let mf = (date.getMinutes() + '').padStart(2, '0'); //分
  let ss = (date.getSeconds() + '').padStart(2, '0'); //秒
  if (symbol == 'symbol') {
    if (bool) {
      return (yy + "-" + mm + "-" + dd + " " + hh + ":" + mf + ":" + ss);
    } else if (!bool) {
      return (yy + "年" + mm + "月" + dd + " 日" + hh + "时" + mf + "分");
    } else {
      return (yy + "-" + mm + "-" + dd + " " + hh + ":" + mf);
    }
  } else if (symbol == 'lately') {
    return (yy + "年" + mm + "月" + dd + "日");
  }

}
