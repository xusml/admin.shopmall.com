import axios, { type AxiosResponse } from "axios";
import Cookie from "js-cookie";
import qs from "qs";
import router from "@/router";
import { ElMessage } from "element-plus";
import { useUserStore } from "@/stores/admin";
import NProgress from "nprogress"

// 扩展类型
declare interface TypeReponse extends AxiosResponse {
    /**
     * 200成功 1006表示失败
     */
    errno: number,
    /**
     * 错误信息
     */
    errmsg: string
}


let baseURL = ''
// process.env.NODE_ENV
//development 开发环境
//production 生产环境(build后的代码)
if (process.env.NODE_ENV === "development") {
    baseURL = "/m.api"; //开发环境
} else {
    baseURL = "http://www.zxwyit.cn:8989/m.api"; //上线后启用
}

const instance = axios.create({
    baseURL,
    headers: {
        'content-type': "application/x-www-form-urlencoded"
    },
    timeout: 5000
})


// 添加请求拦截器
instance.interceptors.request.use(config => {
    if (config.headers) {
        NProgress.start()
        config.headers['AdminToken'] = Cookie.get('token') + ''
    }
    return config;
}, function (error) {
    ElMessage.error('请求错误', error)
    return Promise.reject(error);
});
//添加响应拦截
const METHOD_TYPE = ["_mt=edit", "_mt=create", "_mt=delete", "_mt=update"]
instance.interceptors.response.use(
    async function (response) {
        let data = response.data
        let path = router.currentRoute.value.fullPath
        if (10006 === data.errno) {
            const configData = response.config.data || ""
            var index = METHOD_TYPE.findIndex(item => configData.includes(item))
            // 需要重新登录
            if (-1 == index) {
                ElMessage.error(data.errmsg)
                router.replace({ path: "/login", query: { back: path } })
                return
                // 刷新令牌
            } else {
                const store = useUserStore();
                const { username, password } = store.user
                const loginData = { _gp: "admin", _mt: "login", username, password }
                const { errno, errmsg, data } = await post(loginData)
                console.log(errno);

                if (200 == errno) {
                    Cookie.set("token", data) //重新存储令牌
                } else {
                    ElMessage.error('errmsg')
                    return
                }
                const headersData = response.config.headers || {}
                headersData.AdminToken = data
                let res = await axios.request(response.config)
                if (200 == res.data.errno) {
                    return res.data
                } else {
                    ElMessage.error(res.data.errmsg)
                    return
                }

            }
        }
        NProgress.done()
        return data
    },
    function (error) {
        console.error("请求响应错误", error)
        return Promise.reject(error)
    }
)

function get(params?: object): Promise<TypeReponse> {
    return instance.get('', { params })
}
function post(data: object, params?: object): Promise<TypeReponse> {
    return instance.post('', qs.stringify(data), { params })
}
export function $upload(data: any) {
    return instance.post("/upload/admin", data);
}
export {
    get,
    post
}

