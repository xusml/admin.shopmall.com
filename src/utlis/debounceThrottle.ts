export const Debounce = (fun: any, delay: number) => {
  let t: any = "";
  return (args: any) => {
    const that: any = this;
    const _args = args;
    clearTimeout(t);
    t = setTimeout(function () {
      fun.call(that, _args);
    }, delay);
  };
};