

//友好时间格式
Date.prototype.friendTime = function (num: number) {
    const d = this;
    const now = Date.now()
    let diff = (now - d) / 1000
    if (diff < 30) {//30秒以前
        return '刚刚'
    } else if (diff < 3600) { //60分钟以前
        return Math.floor(diff / 60) + '分钟前'
    } else if (diff < 3600 * 24) {//24小时前
        return Math.floor(diff / 3600) + '小时前'
    } else if (diff < 3600 * 24 * 2) {//2天前
        return '2天前'
    } else {//超过2天显示年日
        return d.formate(num == 0 ? 'yyyy-MM-dd hh:mm' : 'yyyy年MM月dd日 hh时mm分')
    }
}
Date.prototype.formate = function (fmt) {
    let dateObj = {
        'M+': this.getMonth() + 1,
        'd+': this.getDate(),
        'h+': this.getHours(),
        'm+': this.getMinutes(),
        's+': this.getSeconds(),
    }
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, this.getFullYear())
    }
    for (let key in dateObj) {
        let val = dateObj[key] + '';
        if (new RegExp('(' + key + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, val.padStart(2, '0'))
        }
    }
    return fmt
}