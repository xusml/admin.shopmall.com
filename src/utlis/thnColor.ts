function theColorFn(imgs, callback) {
    var imgSrc = imgs
    var upload = document.getElementById('upload')
    const imgEle = document.createElement('img')
    const canvas = document.createElement('canvas')
    imgEle.src = imgSrc
    imgEle.crossOrigin = '';
    imgEle.onload = () => {
        var ctx = canvas.getContext("2d");
        var naturalImgSize = [imgEle.naturalWidth, imgEle.naturalHeight];
        canvas.width = naturalImgSize[0];
        canvas.height = naturalImgSize[1];

        //绘制到canvas
        ctx.drawImage(imgEle, 0, 0);
        //获取imageData：rgba像素点
        var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const leftSectionData = []
        const rightSectionData = []
        const oneLineImgDataLen = canvas.width * 4;

        imgData.data.forEach((colorVal, i) => {
            if (i % oneLineImgDataLen <= 0.5 * oneLineImgDataLen || i % oneLineImgDataLen >= 0.6 * oneLineImgDataLen) {
                const inLeft = i % oneLineImgDataLen <= 0.5 * oneLineImgDataLen
                if (i % 4 === 0) {
                    // 获取rgb均值
                    const curAverageRGB = (imgData.data[i] + imgData.data[i + 1] + imgData.data[i + 2]) / 3;
                    let leftOrRightRef = inLeft ? leftSectionData : rightSectionData;
                    //每个数组里存四个值：本颜色值中的r、g、b的均值，以及r、g、b三个值。
                    //均值一方面用于累加计算本区域的整体均值，然后再跟每个均值对比拿到与整体均值最接近的项的索引，再取该数组里的后三个值：rgb，对应着颜色
                    leftOrRightRef[leftOrRightRef.length] = [curAverageRGB, imgData.data[i], imgData.data[i + 1], imgData.data[i + 2]]
                }
            }
        })
        //generate average rgb
        const averageOfLeft = Math.round(leftSectionData.reduce((_cur, item) => {
            return _cur + item[0]
        }, 0) / leftSectionData.length)
        const averageOfRight = Math.round(rightSectionData.reduce((_cur, item) => {
            return _cur + item[0]
        }, 0) / rightSectionData.length)
        //find the most near color
        const findNearestIndex = (averageVal, arrBox) => {
            let _gapValue = Math.abs(averageVal - arrBox[0])
            let _nearColorIndex = 0
            arrBox.forEach((item, index) => {
                const curGapValue = Math.abs(item - averageVal)
                if (curGapValue < _gapValue) {
                    _gapValue = curGapValue
                    _nearColorIndex = index
                }
            })
            return _nearColorIndex
        }
        const leftNearestColor = leftSectionData[findNearestIndex(averageOfLeft, leftSectionData)]
        const rightNearestColor = rightSectionData[findNearestIndex(averageOfRight, rightSectionData)]
        upload.style.backgroundColor = `rgba(${leftNearestColor[1]},${leftNearestColor[2]},${leftNearestColor[3]},1) 0%,rgba(${rightNearestColor[1]},${rightNearestColor[2]},${rightNearestColor[3]},1)`
        callback(upload.style.backgroundColor)
    }
}
export {
    theColorFn
}