import menus from "@/router/pages";
import index from "./index";
const dynamicrouting =()=>{
    menus.forEach(item=>{
        index.addRoute('index',item)
    })
    index.addRoute({
        path:'/:path(.*)+',
        redirect:'/404'
    })
}
dynamicrouting()