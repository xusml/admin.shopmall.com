import { createRouter, createWebHashHistory } from 'vue-router'
import jsCookie from 'js-cookie'
const Index = () => import('../views/Index.vue')
const Login = () => import('../views/Login.vue')
const Err = () => import('../views/404.vue')

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    // 重定向
    {
      path: '/',
      redirect: "/index"
    },
    // 主页
    {
      path: '/index',
      name: 'index',
      redirect: "/index/home",
      component: Index,
      children: [
        {
          name: 'redirect',
          path: 'redirect/:backpath(.*)',
          meta: { title: '' },
          component: () => import("@/views/redirect/Index.vue")
        },
        {
          name: 'userinfo',
          path: 'userinfo',
          meta: {
            title: "userinfo"
          },
          component: () => import('@/views/admin/Index.vue')
        },
        {
          name: 'changepassword',
          path: 'updatepassword',
          meta: {
            title: "changepassword"
          },
          component: () => import('@/views/admin/UpdatePassword.vue')
        },
      ]
    },

    // 登录
    {
      path: '/login',
      component: Login
    },
    // 404页面

    {
      path: '/404',
      component: Err
    },
    {
      path: '/:path(.*)+',
      component: Err
    }
  ]
})
// 路由守卫
router.beforeEach((to, from, next) => {
  // 路由守卫只能是从登录页面进入，否则就会跳转回登录页
  if (!jsCookie.get("token") && to.path != "/login") {
    router.replace({ path: "/login", query: { back: to.fullPath } })
  }
  next()
})
export default router
