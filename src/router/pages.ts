import Main from "@/views/Main.vue"
export default [

    // ===首页===
    {
        path: 'home',
        name: 'home',
        meta: {
            title: "home",
            icon: 'icon-shouye'
        },
        component: () => import("@/views/Home.vue") //首页
    },
    // ===运维管理===
    {
        path: 'operation',
        name: 'operation',
        component: Main,
        meta: {
            title: "operate.operation",
            icon: 'icon-shuzhuangtu'
        },
        children: [
            {
                path: 'order',
                name: 'order',
                meta: {
                    title: 'operate.order'
                },
                component: () => import("@/views/operation/Order.vue") //订单管理
            },
            {
                path: 'comment',
                name: 'comment',
                meta: {
                    title: 'operate.comment'
                },
                component: () => import("@/views/operation/Comment.vue") //评论管理
            },
            {
                path: 'freight',
                name: 'freight',
                meta: {
                    title: 'operate.freight'
                },
                component: () => import("@/views/operation/Freight.vue") //运费管理
            }
        ]
    },
    // ===商品管理===
    {
        path: 'commodity',
        name: 'commodity',
        component: Main,
        meta: {
            title: 'commodity.commodity',
            icon: 'icon-gouwuche'
        },
        children: [
            {
                path: 'commoditylist',
                name: 'commoditylist',
                meta: {
                    title: 'commodity.commoditylist',
                    show: true,//是否显示在左侧菜单栏，
                },
                component: () => import("@/views/commodity/CommodityList.vue") //商品列表
            },
            {
                path: 'groupbuying',
                name: 'groupbuying',
                meta: {
                    title: 'commodity.groupbuying'
                },
                component: () => import("@/views/commodity/GroupBuying.vue") //团购商品
            },
            {
                path: 'category',
                name: 'category',
                meta: {
                    title: 'commodity.category'
                },
                component: () => import("@/views/commodity/Category.vue") //类目管理
            },
            {//商品添加
                path: "commdadd",
                name: "commdadd",
                component: () => import("@/views/commodity/CommdAdd.vue"),
                meta: {
                    title: "commodity.commdadd",
                    show: false,//是否显示在左侧菜单栏，
                    keepAlive: true
                }
            },
            {//编辑添加
                path: "commdedit/:id",
                name: "commdedit",
                component: () => import("@/views/commodity/CommdEdit.vue"),
                meta: {
                    title: "commodity.commdedit",
                    show: false,//是否显示在左侧菜单栏，
                    keepAlive: true
                }
            }


        ]
    },
    // ===推广管理===
    {
        path: 'extension',
        name: 'extension',
        component: Main,
        meta: {
            title: 'extension.extension',
            icon: 'icon-s-management'
        },
        children: [
            {
                path: 'wide',
                name: 'wide',
                meta: {
                    title: 'extension.wide'
                },
                component: () => import("@/views/extension/Wide.vue") //广告管理
            },
            {
                path: 'coupon',
                name: 'coupon',
                meta: {
                    title: 'extension.coupon'
                },
                component: () => import("@/views/extension/Coupon.vue") //优惠卷管理
            }
        ]
    },
    // ===用户管理===
    {
        path: 'user',
        name: 'user',
        component: Main,
        meta: {
            title: 'user.user',
            icon: 'icon-geren'
        },
        children: [
            {
                path: 'member',
                name: 'member',
                meta: {
                    title: 'user.member'
                },
                component: () => import("@/views/user/Member.vue") //会员管理
            },
            {
                path: 'address',
                name: 'address',
                meta: {
                    title: 'user.address'
                },
                component: () => import("@/views/user/Address.vue") //地址管理
            }
        ]
    },
    // ===系统管理===
    {
        path: 'system',
        name: 'system',
        component: Main,
        meta: {
            title: 'system.system',
            icon: 'icon-shezhi'
        },
        children: [
            {
                path: 'systemconfig',
                name: 'systemconfig',
                meta: {
                    title: 'system.systemconfig'
                },
                component: () => import("@/views/system/SystemConfig.vue") //系统配置
            },
            {
                path: 'administrators',
                name: 'administrators',
                meta: {
                    title: 'system.administrators'
                },
                component: () => import("@/views/system/Administrators.vue") //管理员
            },
            {
                path: 'operationLog',
                name: 'operationLog',
                meta: {
                    title: 'system.operationLog'
                },
                component: () => import("@/views/system/OperationLog.vue") //操作日志
            },
            {
                path: 'role',
                name: 'role',
                meta: {
                    title: 'system.role'
                },
                component: () => import("@/views/system/Role.vue") //角色管理
            }
        ]
    }

]
