import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import "@/router/dynamicrouting"
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import { i18n } from "@/i18n/index";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'nprogress/nprogress.css'
import { useUserStore } from "@/stores/admin";


// pinia状态管理刷新不会丢失数据，引入组件
import PiniaPluginPersistedstate from "pinia-plugin-persistedstate";
const pinia = createPinia()
const app = createApp(App)
pinia.use(PiniaPluginPersistedstate)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(i18n)
app.use(ElementPlus)
app.use(pinia)
app.use(router)

app.mount('#app')
// 自定义指令
app.directive("direct", {
  mounted(el, binding) {
    const store = useUserStore()
    const { perms } = store.user
    console.log(perms)
    const value = binding.value
    var index = perms.findIndex(item => item == value)
    if (-1 == index) {
      el.remove()
    }
  }
})
