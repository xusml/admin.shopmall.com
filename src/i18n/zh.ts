export default {
    home: "首页",
    signout:'退出',
    personalcenter:'个人信息',
    changepassword:'修改密码',
    operate: {
        operation: "运维管理",
        order: "订单管理",
        comment: "评论管理",
        freight: "运费管理",
    },
    commodity: {
        commodity: "商品管理",
        commoditylist: "商品列表",
        groupbuying: "团购商品",
        category: "类目管理",
        commdedit: "商品编辑",
        commdadd: "商品添加"
    },
    extension: {
        extension: "推广管理",
        wide: "广告管理",
        coupon: "优惠卷管理"
    },
    user: {
        user: "用户管理",
        member: "会员管理",
        address: "地址管理"
    },
    system: {
        system: "系统管理",
        systemconfig: "系统配置",
        administrators: "管理员",
        operationLog: "操作日志",
        role: "角色管理"
    }
}