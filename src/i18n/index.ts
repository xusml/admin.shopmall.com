import { createI18n } from "vue-i18n";
import en from "./en";
import zh from "./zh";

// 国际化语言资源
const messages = {
    // 英文
    en: {
        ...en
    },
    // 中文
    zh: {
        ...zh
    }
}
function getLocale() {
    // 如果存储里有这个值就直接用这个值
    let myLocal = localStorage.getItem("language")
    if (myLocal) {
        return myLocal
    }
    // 检索当前浏览器语言是否中文，如果是中文就直接用，否则就用英文
    const localeName = navigator.language.indexOf("zh") !== -1 ? 'zh' : 'en'
    localStorage.setItem("language", localeName)
    return localeName
}
// 通过选项创建 VueI18n 实例
export const i18n = createI18n({
    legacy: false,
    globalInjection: true,  // 全局模式，可以直接使用 $t
    locale: getLocale(),
    messages
})