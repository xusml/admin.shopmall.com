export default {
    home: "Home",
    signout: "SignOut",
    personalpenter: "PersonalCenter",
    changepassword:'changepassword',

    operate: {
        operation: "order",
        order: "OrderManage",
        comment: "CommentManage",
        freight: "FreightManage",
    },
    commodity: {
        commodity: "CommodityManage",
        commoditylist: "ProductList",
        groupbuying: "GroupPurchase",
        category: "GategoryManage",
        commdedit: "commdedit",
        commdadd: "commdadd"
    },
    extension: {
        extension: "PromotionManage",
        wide: "AdvertisingManage",
        coupon: "CouponManage"
    },
    user: {
        user: "UserManage",
        member: "MemberManage",
        address: "AddressManage"
    },
    system: {
        system: "SystemManage",
        systemconfig: "SystemConfig",
        administrators: "Administrators",
        operationLog: "OperationLog",
        role: "RoleManage"
    }
}