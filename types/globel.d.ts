import type { number } from "@intlify/core-base"

interface Date {
    friendTime(num?: number): string,
    formate(data): string
}
/**
 * 事件源定义
 */
declare interface EventTarget {
    scrollTop: number
}
/**
 * 操作日志
 */
declare interface TypeLog {
    id: number,
    adminId: number,
    group: string,
    method: string,
    gmtCreate?: string | number
}
/**
 * 角色管理
 */
declare interface TypeUser {
    id: number,
    name: string,
    desc: string,
    status: number
}
/**
 * 管理员
 */
declare interface TypeAdmin {
    name: string
    value: string | number | symbol | undefined
    gmtCreate: number,
    gmtLastLogin: string,
    gmtUpdate: number,
    id: number,
    lastLoginIp: string,
    phone: number,
    realname: string,
    roleIds: Array
    status: number,
    username: string
}
/**
 * 管理员角色信息
 */
declare interface TypeAdminData {
    adminList: TypeAdmin[],
    userList: TypeAdmin[]
}
/**
 * 地址管理
 */
declare interface TypeAddress {
    address: string,
    city: string,
    consignee: string,
    county: string,
    defaultAddress: number,
    gmtCreate: number,
    gmtUpdate: number,
    id: number,
    phone: number,
    province: string,
    userId: number
}
/**
 * 会员管理
 */
declare interface TypeVipAdmin {
    nickname: string,
    avatarUrl: string,
    birthday?: string | number,
    gender: number,
    gmtCreate?: string | number,
    gmtLastLogin?: string | number,
    gmtUpdate: number,
    id: number,
    lastLoginIp: string,
    level: number,
    loginType: number,
    openId: string,
    phone: string,
    userId: number
    status: number
}
/**
 * 编辑会员管理
 */
declare interface TypeEdit {
    nickname: string,
    phone: string,
    gender: string,
    level: string,
    status: string,
    id: number
}
/**
 * 优惠卷管理
 */
declare interface TypeCoupon {
    birthday: string,
    categoryId: number,
    categoryTitle: string,
    description: string,
    discount: number,
    gmtEnd?: string | undefined,
    gmtLastLogin: string,
    gmtStart?: string | undefined,
    id: number,
    limit: number,
    min: number,
    status?: number | string,
    surplus: string,
    title?: number | string,
    total: number,
    type?: number | string,
}
/**
 * 修改优惠券信息
 */
declare interface TypeEditCoupon {
    id?: number | string
    title: string,//优惠卷标题
    type: string,//优惠卷类别
    description: string,//优惠卷描述
    total: number,//优惠卷总数
    limit: number,//用户限制领取
    discount: number,//优惠价格
    min: number,//满足优惠的最低价格
    status?: number | string,//优惠卷状态
    categoryId: string,//优惠类别
    days: string,//优惠时长
    gmtStart?: string | number,//优惠开始时间
    gmtEnd?: string | number,//优惠结束时间
}
/**
 * 类目树
 */
declare interface TypeCategory {
    childrenList: Array,
    fullName?: string | number,
    gmtCreate?: number | string,
    gmtUpdate?: number | string,
    id: number,
    level: number,
    parentId: number,
    title: string
}
/**
 * 广告管理
 */
declare interface TypeAdvert {
    id?: string | number,
    color?: string,
    gmtCreate?: number | string,
    gmtUpdate?: number | string,
    id: number,
    imgUrl: string,
    status: number | string,
    title: string,
    type: number | string,
    unionType: number | string,
    unionValue: string | number,
    adId?: string | number
}
/**
 * 团购管理
 */
declare interface TypeGroupying {
    automaticRefund: number,
    buyerNum: number,
    categoryId: number
    description: string,
    detail: string,
    freightTemplateId: number,
    gmtCreate: number,
    gmtEnd?: string | number,
    gmtStart?: string | number,
    gmtUpdate: number,
    groupShopSkuDTOList: Array,
    id: number,
    img: string,
    maxPrice: number,
    minNum: number,
    minPrice: number,
    originalPrice: number,
    price: number,
    sales: number,
    spuId: number,
    status: number,
    title: string,
    unit: string,
    vipPrice: number
}
/**
 * 团购管理内层表格
 */
declare interface TypeSkuidlist {
    barCode: string,
    gmtCreate: number,
    gmtUpdate: number,
    groupShopId: number,
    id: number,
    img: string,
    originalPrice: number,
    price: number,
    skuGroupShopPrice: number,
    skuId: number,
    spuId: number,
    stock: number,
    title: string,
    groupres?: number | string,
    vipPrice: number
}
/**
 * 添加团购
 */
declare interface TypeAddGroup {
    spuId?: number | string,          //spuId
    gmtStart?: number | string,       //团购开始时间戳
    gmtEnd?: number | string,         //团购结束时间戳
    minNum: number,                   //团购最低人数
    automaticRefund?: string | number,//团购人数未满是否自动退款
    groupShopSkuList: Array           //团购sku链表
}
/**
 * 团购内层表格
 */
declare interface TypeSkuList {
    barCode?: string | number,
    gmtCreate: number,
    gmtUpdate: number,
    id: number,
    img: string,
    originalPrice: number,
    price: number,
    specification: string,
    spuId: number,
    stock: number,
    title: string,
    vipPrice: number,
    weight: number,
    groupres?: string | number
}
/**
 * 团购数组
 */
declare interface TypeSkuLists {
    skudList: TypeSkuList[]
}
/**
 * 商品管理
 */
declare interface TypeCommodity {
    activityId: number,
    activityType: number,
    categoryId: number,
    description: string,
    freightTemplateId: number,
    gmtActivityEnd: number,
    gmtActivityStart: number,
    gmtCreate: number,
    gmtUpdate: number,
    id: number,
    img: string,
    originalPrice: number,
    price: number,
    sales: number,
    skuList: Array,
    status: number,
    title: string,
    unit: string,
    vipPrice: number,
}
/**
 * 商品添加
 */
declare interface TypeGoods {
    title: string,             //商品名称
    originalPrice: number,     //原始价格
    price: number,             //现在价格
    vipPrice: number,          //vip价格
    stock: number,             //剩余库存
    freightTemplateId: string, //运费模板
    unit: string,              //商品单位
    categoryId: string,        //类目
    description: string,       //商品简介
    status: number,            //上架下架
    detail: string,            //商品详细介绍
    attributeList: Array,      //商品参数
    specificationList: Array,  //规格维度
    skuList: Array,            //批量定价
    img: string,               //第一张图片
    imgList: Array
}
/**
 * 拼接数据
 */
declare interface TypeConcat {
    price: string,
    vipPrice: string,
    originalPrice: string,
    stock: string,
    barCode: string,
    weight: string,
    img: string,
    title: Array,
    specification: Array
}
// 分页
declare interface TypePage {
    _pageNo: number,
    _pageSize: number,

}