export class ClassPagination {
    /**
     *当前页码 
     */
    pageNo: number = 1 //当前页码
    /** 
    *每页数量
    */
    pageSize: number = 10 //每页显示的条数
    /**
     * 记录总数
     */
    total: number = 1 //记录总数
}